# -*- coding: utf-8 -*-
"""
ファジィ推論のデータクラス
"""

class FazzyData:
    
    # numはfazzy推論の関数個数
    def CreateFazzy(self, num, maxPoint):
        """ファジィ推論の各関数を出力する
        
        num:各推論関数の数
        maxPoint:ファジィ推論x軸最大値
        """
        fazzyList = []
        for i in range (num+1):
            fazzyList.append(maxPoint*i/num)
            print (fazzyList)
        FazzyData().m_createKeisu(fazzyList)

    def m_createKeisu(self, fazzyList):
        """各推論の関数を定義する
    
        fazzyList:各直線が通るx軸の値が入る
        """
        for i in range (len(fazzyList)-1):
            keisuA = (1.0-0.0)/(fazzyList[i+1]-fazzyList[i])
            keisuB = (keisuA * -fazzyList[i]) + 0.0
            print(keisuA)
            
fazzy = FazzyData()
m_fazzyList = fazzy.CreateFazzy(4, 100)
        
    